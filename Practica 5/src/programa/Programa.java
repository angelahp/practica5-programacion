package programa;

import java.time.LocalDate;
import java.util.Scanner;

import clases.*;

/**
 * 
 * @author Angela Hernandez
 *
 */

public class Programa {

	static Scanner scanner = new Scanner(System.in);
	
	static int opcion;
	
	static Pastelito pastelillo;
	static Bizcochito bizcochillo;
	static Magdalenita magdalenilla;

	public static void main(String[] args) {

		do {
			mostrarOpciones();
			opcion = scanner.nextInt();
			ejecutarOpciones(opcion);

		} while (opcion != 0);
	}

	/** 
	 * Este metodo muestra por pantalla las opciones (menu)
	 * 
	 * */
	
	public static void mostrarOpciones() {

		System.out.println("\n------  BIENVENID@ A LA PASTELER�A HERNANDEZ  ------\n");

		System.out.println("\n1. Crear objeto de Pastelito");
		System.out.println("2. Crear objeto de Bizcochito");
		System.out.println("3. Crear objeto de Magdalenita");
		System.out.println("4. Mostrar atributos de Pastelito");
		System.out.println("5. Mostrar atributos de Bizcochito");
		System.out.println("6. Mostrar atributos de Magdalenita");
		System.out.println("7. Cambiar forma del Bizcochito");
		System.out.println("8. Cambiar topping de la Magdalenita");
		System.out.println("9. Precio del Pastelito (Oferta los Viernes)");
		System.out.println("10. Calcular presupuesto para realizar Pastelito");
		System.out.println("11. Cual tiene mas calorias");
		System.out.println("12. Crear objetos automaticamente");

		System.out.println("\nIntroduce una opcion (0 para salir): ");

	}

	/** 
	 * Este metodo ejecuta las opciones (menu)
	 *@param opcion introducida por teclado 
	 * */
	
	public static void ejecutarOpciones(int opcion) {

		scanner.nextLine();

		switch (opcion) {

		case 1: /* Crear objeto Pastelito*/

			System.out.println(" * Para crear objeto de Pastelito, rellena los siguientes datos:");

			System.out.println("Introduce nombre del Pastelito: ");
			String nombre = scanner.nextLine();

			System.out.println("Introduce textura del Pastelito: ");
			String textura = scanner.nextLine();

			System.out.println("Introduce fecha de creacion del Pastelito: ");
			String fechaP = scanner.nextLine();
			LocalDate fechaCreacionP = LocalDate.parse(fechaP);

			System.out.println("Introduce peso del Pastelito: ");
			Double peso = scanner.nextDouble();

			System.out.println("Introduce precio del Pastelito: ");
			Double precio = scanner.nextDouble();

			System.out.println("Introduce calorias del Pastelito: ");
			Double calorias = scanner.nextDouble();

			pastelillo = new Pastelito(nombre, textura, fechaCreacionP, peso, precio, calorias);

			System.out.println(" ** Objeto de Pastelito creado con EXITO **\n ");
			break;

		case 2: /* Crear objeto Bizcochito */

			System.out.println(" * Para crear objeto de Bizcochito, rellena los siguientes datos:");

			System.out.println("Introduce nombre del Bizcochito: ");
			nombre = scanner.nextLine();

			System.out.println("Introduce textura del Bizcochito: ");
			textura = scanner.nextLine();

			System.out.println("Introduce fecha de creacion del Bizcochito: ");
			String fechaB = scanner.nextLine();
			LocalDate fechaCreacionB = LocalDate.parse(fechaB);

			System.out.println("Introduce peso del Bizcochito: ");
			peso = scanner.nextDouble();

			System.out.println("Introduce precio del Bizcochito: ");
			precio = scanner.nextDouble();

			System.out.println("Introduce calorias del Bizcochito: ");
			calorias = scanner.nextDouble();

			scanner.nextLine();

			System.out.println("Introduce fruta del Bizcochito: ");
			String fruta = scanner.nextLine();

			System.out.println("Introduce forma del Bizcochito: ");
			String forma = scanner.nextLine();

			bizcochillo = new Bizcochito(nombre, textura, fechaCreacionB, peso, precio, calorias, fruta, forma);

			System.out.println(" ** Objeto de Bizcochito creado con EXITO **\n ");
			break;

		case 3: /* Crear objeto Magdalenita */

			System.out.println(" * Para crear objeto de Magdalenita, rellena los siguientes datos:");

			System.out.println("Introduce nombre del Magdalenita: ");
			nombre = scanner.nextLine();

			System.out.println("Introduce textura del Magdalenita: ");
			textura = scanner.nextLine();

			System.out.println("Introduce fecha de creacion del Magdalenita: ");
			String fechaM = scanner.nextLine();
			LocalDate fechaCreacionM = LocalDate.parse(fechaM);

			System.out.println("Introduce peso del Magdalenita: ");
			peso = scanner.nextDouble();

			System.out.println("Introduce precio del Magdalenita: ");
			precio = scanner.nextDouble();

			System.out.println("Introduce calorias del Magdalenita: ");
			calorias = scanner.nextDouble();

			scanner.nextLine();

			System.out.println("Introduce envoltorio del Magdalenita: ");
			String envoltorio = scanner.nextLine();

			System.out.println("Introduce topping del Magdalenita: ");
			String topping = scanner.nextLine();

			magdalenilla = new Magdalenita(nombre, textura, fechaCreacionM, peso, precio, calorias, envoltorio,
					topping);
			System.out.println(" ** Objeto de Magdalenita creado con EXITO **\n ");

			break;

		case 4: /* Mostrar atributos Pastelito */
			System.out.println("\t-> Atributos de Pastelito: <-\n" + pastelillo.toString());
			break;

		case 5: /* Mostrar atributos Bizcochito */
			System.out.println("\t-> Atributos de Bizcochito: <-\n" + bizcochillo.toString());
			break;

		case 6: /* Mostrar atributos Magdalenita */
			System.out.println("\t-> Atributos de Magdalenita: <-\n" + magdalenilla.toString());
			break;

		case 7: /* Cambiar forma Bizcochito  */
			bizcochillo.setForma(bizcochillo.cambiarForma());
			break;

		case 8: /* Cambiar topping Magdalenita  */
			magdalenilla.setTopping(magdalenilla.cambiarTopping());
			break;

		case 9: /* Calcular calorias totales */
			pastelillo.calcularCaloriasTotales();
			break;

		case 10: /* Calcula precio de todos los ingredientes */
			pastelillo.ingredientes();
			break;

		case 11: /* Mostrar el que m�s calorias tiene */
			if (pastelillo.calcularCaloriasTotales() > bizcochillo.calcularCaloriasTotales()
					&& pastelillo.getCalorias() > magdalenilla.calcularCaloriasTotales()) {

				System.out.println("El Pastelito es el que tiene mas calorias. Tiene " 
				+ pastelillo.calcularCaloriasTotales());
				System.out.println(pastelillo.toString());

			} else if (bizcochillo.calcularCaloriasTotales() > pastelillo.calcularCaloriasTotales()
					&& bizcochillo.getCalorias() > magdalenilla.calcularCaloriasTotales()) {

				System.out.println("El Bizcochito es el que tiene mas calorias. Tiene " 
				+ bizcochillo.calcularCaloriasTotales());
				System.out.println(bizcochillo.toString());

			} else if (magdalenilla.calcularCaloriasTotales() > pastelillo.calcularCaloriasTotales()
					&& magdalenilla.getCalorias() > bizcochillo.calcularCaloriasTotales()) {

				System.out.println("La Magdalenita es el que tiene mas calorias. Tiene " 
				+ magdalenilla.calcularCaloriasTotales());
				System.out.println(magdalenilla.toString());

			}
			break;
		/*
		case 12: //crea automaticamente los objetos 

			fechaP = "2020-01-10";
			fechaCreacionP = LocalDate.parse(fechaP);
			fechaB = "2020-02-12";
			fechaCreacionB = LocalDate.parse(fechaB);
			fechaM = "2019-05-14";
			fechaCreacionM = LocalDate.parse(fechaM);

			pastelillo = new Pastelito("Pastel de chocolate", "esponjoso", fechaCreacionP, 305, 3, 600);
			bizcochillo = new Bizcochito("Bizcocho de limon", "esponjoso", fechaCreacionB, 620, 3.45, 540, "limon",
					"rectangular");
			magdalenilla = new Magdalenita("Magdalenas con pepitas de chocolate", "empalagosa", fechaCreacionM, 100,
					2.89, 210, "de silicona", "Pepitas de Chocolate");

			break;
		*/
			
		case 0:
			System.exit(0);

		default:
			System.out.println("Opcion introducida no valida.");
			break;

		}

	}

}
