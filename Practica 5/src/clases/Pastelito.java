package clases;

import java.time.LocalDate;
import java.util.Scanner;

/**
 * Clase en la que se guarda la informacion de distintos Pastelitos
 * 
 * @author Angela Hernandez
 *
 */

public class Pastelito {

	static Scanner scan = new Scanner(System.in);

	// Atributos:

	protected String nombre;
	protected String textura;
	protected LocalDate fechaCreacion;
	protected double peso;
	protected double precio;
	protected double calorias;

	/**
	 * Constructores
	 * 
	 */

	public Pastelito() {

		this.nombre = "";
		this.textura = "";
		this.peso = 0;
		this.precio = 0;
		this.calorias = 0;

	}

	/**
	 * Da de alta un nuevo Pastelito
	 * 
	 * @param nombre        del Pastelito o Herederos
	 * @param textura       del Pastelito o Herederos
	 * @param fechaCreacion del Pastelito o Herederos
	 * @param peso          del Pastelito o Herederos
	 * @param precio        del Pastelito o Herederos
	 * @param calorias      del Pastelito o Herederos
	 * 
	 */

	public Pastelito(String nombre, String textura, LocalDate fechaCreacion, double peso, double precio,
			double calorias) {

		this.nombre = nombre;
		this.textura = textura;
		this.fechaCreacion = fechaCreacion;
		this.peso = peso;
		this.precio = precio;
		this.calorias = calorias;

	}

	// Setters y Getters

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTextura() {
		return textura;
	}

	public void setTextura(String textura) {
		this.textura = textura;
	}

	public LocalDate getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFecha(LocalDate fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public double getCalorias() {
		return calorias;
	}

	public void setCalorias(double calorias) {
		this.calorias = calorias;
	}

	//metodo toString

	@Override
	public String toString() {

		return "\nNombre: " + getNombre() + "\nTextura: " + getTextura() + "\nFecha Creacion: " + getFechaCreacion()
				+ "\nPeso: " + getPeso() + "\nPrecio: " + getPrecio() + "\nCalorias: " + getCalorias() + "\n";
	}

	/**
	 * Metodo que calcula calorias totales
	 * 
	 * @return caloriasTotales
	 */
	
	public double calcularCaloriasTotales() {

		final double AZUCARGLASS = 0.75;

		double caloriasTotales = calorias + AZUCARGLASS;
		return caloriasTotales;

	}
	/**
	 * Metodo que calcula el precio segun el dia de la semana de su creacion
	 * 
	 */

	public void vender() {

		final double DESCUENTO = 0.15;

		if (fechaCreacion.getDayOfWeek().name().equals("Friday")) {

			System.out.println("�Que sorpresa! Este Pastelito vale: " + (precio - (precio * DESCUENTO)));
			System.out.println("Antes valia: " + precio);

		} else {
			System.out.println("El precio del Pastelito es: " + precio);
		}

	}
	
	/**
	 * Metodo que calcula el precio de los ingredientes empleados
	 * 
	 */

	public void ingredientes() {

		System.out.println("Introduce la cantidad de ingredientes necesarios para hacer un Pastelito: ");
		int cantidad = scan.nextInt();
		scan.nextLine();

		String[] ingredientes = new String[cantidad];
		double[] precioIngredientes = new double[cantidad];

		for (int i = 0; i < ingredientes.length; i++) {
			System.out.println("Introduce el nombre del ingrediente: ");
			ingredientes[i] = scan.nextLine();

			do {
				System.out.println("Introduce lo que ha costado: ");
				precioIngredientes[i] = scan.nextDouble();
				System.out.println();

				scan.nextLine();

			} while (precioIngredientes[i] < 0);

		}

		double precioFinal = 0;
		System.out.println("\n---> Los ingredientes necesarios son:  <---");

		for (int i = 0; i < ingredientes.length; i++) {
			System.out.println(" � " + ingredientes[i]);
			precioFinal += precioIngredientes[i];
		}

		System.out.println("Los ingredientes especificados anteriormente han costado: " + precioFinal + "� \n");

	}

}
