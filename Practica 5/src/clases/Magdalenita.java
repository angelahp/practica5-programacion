package clases;

import java.time.LocalDate;
import java.util.Scanner;

/**
 * 
 * @author Angela Hernandez
 *
 */

public class Magdalenita extends Pastelito{
	
	//Atributos
	
	private String envoltorio;
	private String topping;
	
	//Setters y Getters
	
	public String getEnvoltorio() {
		return envoltorio;
	}
	
	public void setEnvoltorio(String envoltorio) {
		this.envoltorio = envoltorio;
	}
	
	public String getTopping() {
		return topping;
	}
	
	public void setTopping(String topping) {
		this.topping = topping;
	}
	
	/**
	 * Constructores
	 * 
	 * */
	
	public Magdalenita() {
			super();
			this.envoltorio = "";
			this.topping = "";
			
	}
	
	/** 
	 * Constructor con todos los atributos de la clase de la que hereda y sus atributos
	 * @param calorias = 1.5 
	 * 
	 * */
		
	public Magdalenita (String nombre, String textura, LocalDate fechaCreacion, double peso, double precio,double calorias,
			String envoltorio, String topping) {
			
			super(nombre, textura, fechaCreacion, peso, precio, calorias);
			this.calorias = 1.5;
			this.envoltorio = envoltorio;
			this.topping = topping;
			
	}
	
	//metodo toString
	
	@Override
	public String toString() {
		return "Atributos nuevos: \nEnvoltorio: " + envoltorio + "\nTopping=" + topping  
				+ "\nAtributos comunes: " + super.toString();
	}
	
	
	/*Metodo propio para cambiar el Topping*/
	
	static Scanner scanner = new Scanner(System.in);
	
	public String cambiarTopping() {
		
		
		System.out.println("Introduce el topping que quieres poner: ");
		String nuevoTopping = scanner.nextLine();
		
		return nuevoTopping;
		
	}
	
	
	
		
}
