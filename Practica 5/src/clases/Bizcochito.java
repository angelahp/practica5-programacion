package clases;

import java.time.LocalDate;
import java.util.Scanner;

/**
 * 
 * @author Angela Hernandez
 *
 */

public class Bizcochito extends Pastelito{
	
	static Scanner scan = new Scanner(System.in);	
	//Atributos
	
	private String fruta;
	private String forma;
	
	//Setters y Getters
	
	public String getFruta() {
		return fruta;
	}
	
	public void setFruta(String fruta) {
		this.fruta = fruta;
	}
	
	public String getForma() {
		return forma;
	}
	
	public void setForma(String forma) {
		this.forma = forma;
	}
	
	//Constructores
	
	public Bizcochito () {
		super();
		this.fruta = "";
		this.forma = "";
		
	}
	
	public Bizcochito (String nombre, String textura, LocalDate fechaCreacion, double peso, double precio,double calorias,
			String fruta, String forma ) {
		
		super(nombre, textura,fechaCreacion, peso, precio, calorias );
		this.fruta = fruta;
		this.forma = forma;
	}
	
	// metodo toString
	
	@Override
	public String toString() {
		return "Atributos nuevos: \nFruta: " + fruta + "\nForma: " + forma  
				+ "\nAtributos comunes: " + super.toString();
	}
	
	/**
	 *  Metodo propio para cambiar forma
	 *  @return nuevaForma
	 *  */
	
	public String cambiarForma() {
		
		System.out.println("Introduce la forma que quieres darle al bizcocho: ");
		String nuevaForma = scan.nextLine();
		
		return nuevaForma;
		
	}
}
	